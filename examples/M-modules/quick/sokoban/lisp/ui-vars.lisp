;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*board*
   #:*level*
   #:*rotate-player*
   #:*wiggle-box*
   #:*zoom-board-in*
   #:*zoom-board-out*))

(provide :ui-vars)

(in-package :ui)

(defparameter *board*          "board")          ; Rectangle           "qml/sokoban.qml"
(defparameter *rotate-player*  "rotate_player")  ; RotationAnimation   "qml/items/player.qml"
(defparameter *zoom-board-in*  "zoom_board_in")  ; ScaleAnimator       "qml/sokoban.qml"
(defparameter *zoom-board-out* "zoom_board_out") ; ScaleAnimator       "qml/sokoban.qml"
(defparameter *wiggle-box*     "wiggle_box")     ; SequentialAnimation "qml/items/box2.qml"
(defparameter *level*          "level")          ; Slider              "qml/sokoban.qml"
